import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {

  posts = [
    { 
    id: 1,
    title: "Mon premier post", 
    content:"bienvenue à Simplon 1", 
    loveIts: "Love it",
    created_at: "data",
    postStatus: "éteint",
  }, 

    { 
    id: 2,
    title: "Mon deuxième post" , 
    content:"bienvenue à Simplon 2", 
    loveIts: "Love it",
    created_at: "date",
    postStatus: "allumé" },

    {
    id: 3,  
    title: "Encore un post" , 
    content:"Mon post préférer", 
    loveIts: "Love it",
    created_at: "date",
    postStatus: "éteint" },
  ];

  getPostById (id: number){
    const post = this.posts.find(
      (postObjet)=>{
        return postObjet.id==id;
      }
    );
    return post;
  }
  switchOnAll(){
    for (let annonce of this.posts){
      annonce.postStatus = "allumé"

    } 
  }

  switchOffAll(){
    for (let annonce of this.posts){
      annonce.postStatus = "éteint"
    } 

  }

  switchOnOne(index: number){
    this.posts[index].postStatus="allumé"
  }

  switchOffOne(index: number){
    this.posts[index].postStatus="éteint"
  }

  constructor() { }

}

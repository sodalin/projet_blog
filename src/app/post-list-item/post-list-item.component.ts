import { Component, Input, OnInit} from '@angular/core';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title:string;
  @Input() content:string;
  @Input() loveIts:string;
  @Input() created_at:string;
  @Input() postStatus:string;
  @Input() indexOfannonce:number;
  @Input() id:number;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  getStatus(){
    return this.postStatus;
  }
  getColor() {
    if(this.postStatus === 'allumé') {
      return 'green';
    } else if(this.postStatus === 'éteint') {
      return 'red';
    }
}

onSwitchOn(){
  this.postsService.switchOnOne(this.indexOfannonce);
}

onSwitchOff(){
  this.postsService.switchOffOne(this.indexOfannonce);
}
}

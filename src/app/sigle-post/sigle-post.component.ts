import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-sigle-post',
  templateUrl: './sigle-post.component.html',
  styleUrls: ['./sigle-post.component.scss']
})
export class SiglePostComponent implements OnInit {

  id:number;
  title:string;
  content:string ;
  loveIts:string;
  created_at:string;
  postStatus:string;

  // id:number;
  // title:string = "Une nouveau post";
  // content:string ="love it 1";
  // loveIts:string = "j'aime"; 
  // created_at:string = "la date"
  // postStatus:string ="Le statut";

  // Afficher par les id

  constructor(private postsService: PostsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const id  = this.route.snapshot.params['id'];
    this.title = this.postsService.getPostById(+id).title;
    this.content = this.postsService.getPostById(+id).content;
    this.loveIts = this.postsService.getPostById(+id).loveIts;
    this.created_at = this.postsService.getPostById(+id).created_at;
    this.postStatus = this.postsService.getPostById(+id).postStatus;
  }

} 

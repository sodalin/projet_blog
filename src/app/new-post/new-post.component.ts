import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  isAuth = false;

  lastUpdate = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(
      () => {
        resolve(date);
      }, 1000
    );
  })
  posts: any[];  
      constructor(private postsService: PostsService) {
      setTimeout(
        () => {
          this.isAuth = true;
        }, 4000
      );
    }
  ngOnInit() {
    this.posts = this.postsService.posts;

  }
  loveIt() {
    this.postsService.switchOnAll();
  }
  
  dontLoveIt() {
    this.postsService.switchOffAll();
    }  

 
}

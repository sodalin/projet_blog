import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from './services/auth.guard.service';
import { NewPostComponent } from './new-post/new-post.component';
import { RouterModule, Routes } from "@angular/router";
import { AuthService } from './services/auth.service';
import { SiglePostComponent } from './sigle-post/sigle-post.component';
import { PageErreurComponent } from './page-erreur/page-erreur.component';
import { PostsService } from './posts.service';
import { AjouterNouveauPostComponent } from './ajouter-nouveau-post/ajouter-nouveau-post.component';


const appRoute: Routes = [
  {path: 'posts', component: NewPostComponent},
  {path: 'posts/:id', canActivate:[AuthGuard], component: SiglePostComponent},
  {path: 'auth', component: AuthComponent},
  {path: '', component:NewPostComponent},
  {path: 'page-404', component: PageErreurComponent},
  {path: '**', redirectTo: '/page-404'},
];

@NgModule({
  declarations: [
    AppComponent,
    PostListItemComponent,
    AuthComponent,
    NewPostComponent,
    SiglePostComponent,
    PageErreurComponent,
    AjouterNouveauPostComponent,
    
  ],

  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoute)
  ],
  providers: [PostsService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }


